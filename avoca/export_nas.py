import warnings

from avoca.bindings.ebas import *

warnings.warn(
    "avoca.export_nas is deprecated. Use the new module avoca.bindings.ebas instead."
)
