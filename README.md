# avoca 

Python package for Quality assessement of data.

Official name is `@voc@` but it is called `avoca` for simplicity.

## Documentation 

A documentation can be found at (http://avoca.readthedocs.io/)


## License

This project is licensed under the MIT License.

It is managed by the a group of researchers at [Empa](https://www.empa.ch/web/s503/climate-gases)