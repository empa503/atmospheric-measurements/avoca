# Bindings to EBAS 

EBAS handles the data using files in a `.nas` format.

## Requirements

The ebas-io packages must be installed.
Please follow the instructions: https://git.nilu.no/ebas/ebas-io/-/wikis/home#installation 


## Flags 

EBAS has a flag system: https://projects.nilu.no/ccc/flags/flags.html 

We tried to translate the flags to the Avoca flags, but it is not guaranteed 
that all flags have the same meaning.

## API


```{eval-rst}  
.. automodule:: avoca.bindings.ebas
    :members:
```