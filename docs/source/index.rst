.. @voc@ documentation master file, created by
   sphinx-quickstart on Tue Apr  2 10:38:23 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to @voc@'s documentation!
=================================

@voc@ is a python programm for data quality assessment (QA).

The main concept applied is flagging the data when some specific pattern has been found.
Currently @voc@ supports the following quality assessment methods:

* outliers  
* correlated patterns 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   qa_classes
   bindings/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
