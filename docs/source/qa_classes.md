(Models)=
# QA Classes

## RetentionTimeChecker
```{eval-rst} 
.. autoclass:: avoca.qa_class.rt.RetentionTimeChecker
```

## ExtremeValues
```{eval-rst} 
.. autoclass:: avoca.qa_class.zscore.ExtremeValues
```

## ExtremeConcentrations
```{eval-rst} 
.. autoclass:: avoca.qa_class.concs.ExtremeConcentrations
```

## XY_Correlations
```{eval-rst} 
.. autoclass:: avoca.qa_class.zscore.XY_Correlations
```

## TestAssigner
```{eval-rst} 
.. autoclass:: avoca.qa_class.test.TestAssigner
```

